#include <iostream>  
#include <sstream> // use stringstream class  

unsigned long getN(char *arg) {
      
    unsigned long n; // declare integer variable  
      
    /* use stringstream class to declare a stream object to insert a string and then fetch as integer type data. */  
       
     std::stringstream obj;  
     obj << arg; // insert data into obj  
     obj >> n; // fetch integer type data  
     return n;
}

unsigned long p = 17;

int main(int argc, char *argv[]) {
    unsigned long n = getN(argv[1]);
    unsigned long count = 0;
    while (n % p == 0) {
        count++;
        n /= p;
    }
    std::string log = "Shame I can't put a template in this cpp file :(";
    std::cout << p << "," << count << "," << log;
    return 0;
}